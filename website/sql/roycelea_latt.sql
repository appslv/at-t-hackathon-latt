-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2012 at 11:26 PM
-- Server version: 5.5.23
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `roycelea_latt`
--

-- --------------------------------------------------------

--
-- Table structure for table `att-user-location`
--

CREATE TABLE IF NOT EXISTS `att-user-location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT 'User that coords belong to',
  `coords` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lat, Long coords.',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of insertion.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `att-user-location`
--

INSERT INTO `att-user-location` (`id`, `userid`, `coords`, `updatetime`) VALUES
(1, 1, '50,-150', '2012-11-18 00:33:05'),
(2, 1, ',', '2012-11-18 00:51:26'),
(3, 1, '49,-149', '2012-11-18 01:34:46'),
(4, 1, '49,-148', '2012-11-18 01:36:27'),
(5, 1, '49,-148', '2012-11-18 01:37:07'),
(6, 1, '49,-148', '2012-11-18 01:39:10'),
(7, 1, '49,-148', '2012-11-18 01:43:46'),
(8, 1, '49,-148', '2012-11-18 01:45:50'),
(9, 1, '49,-148', '2012-11-18 01:46:56'),
(10, 1, '49,-148', '2012-11-18 01:47:42'),
(11, 2, '49,-148', '2012-11-18 01:56:48'),
(12, 2, '49,-148', '2012-11-18 02:40:41'),
(13, 2, '50,-150', '2012-11-18 02:45:54'),
(14, 2, '50,-150', '2012-11-18 02:45:56'),
(15, 2, '50,-150', '2012-11-18 02:45:57'),
(16, 2, '50,-150', '2012-11-18 02:45:57'),
(17, 2, '50,-150', '2012-11-18 02:46:08'),
(18, 2, '50,-150', '2012-11-18 02:46:34'),
(19, 2, '50,-150', '2012-11-18 02:50:06'),
(20, 2, '50,-150', '2012-11-18 02:50:43'),
(21, 2, '50,-150', '2012-11-18 02:51:41'),
(22, 2, '50,-150', '2012-11-18 02:52:39'),
(23, 2, '50,-150', '2012-11-18 02:53:29'),
(24, 2, '50,-150', '2012-11-18 02:57:16'),
(25, 2, '50,-150', '2012-11-18 02:57:30'),
(26, 2, '50,-150', '2012-11-18 02:57:32'),
(27, 2, '50,-150', '2012-11-18 02:57:33'),
(28, 2, '50,-150', '2012-11-18 02:59:39'),
(29, 2, '50,-150', '2012-11-18 03:00:05'),
(30, 2, '50,-150', '2012-11-18 03:00:52'),
(31, 2, '50,-150', '2012-11-18 03:01:57'),
(32, 2, '50,-150', '2012-11-18 03:10:10'),
(33, 2, '50,-150', '2012-11-18 03:17:22'),
(34, 2, '50,-150', '2012-11-18 03:18:27'),
(35, 2, '50,-150', '2012-11-18 03:20:13'),
(36, 2, '50,-150', '2012-11-18 03:20:59'),
(37, 2, '50,-150', '2012-11-18 03:21:20'),
(38, 2, '50,-150', '2012-11-18 03:24:31'),
(39, 2, '50,-150', '2012-11-18 03:27:25'),
(40, 2, '50,-150', '2012-11-18 03:33:26'),
(41, 2, '50,-150', '2012-11-18 03:33:38'),
(42, 2, '50,-150', '2012-11-18 03:33:41'),
(43, 2, '50,-150', '2012-11-18 03:33:45'),
(44, 2, '50,-150', '2012-11-18 03:33:53'),
(45, 2, '50,-150', '2012-11-18 03:34:06'),
(46, 2, '50,-150', '2012-11-18 03:34:54'),
(47, 2, '50,-150', '2012-11-18 03:34:57'),
(48, 2, '50,-150', '2012-11-18 03:34:58'),
(49, 2, '50,-150', '2012-11-18 03:35:06'),
(50, 2, '50,-150', '2012-11-18 03:37:47'),
(51, 2, '50,-150', '2012-11-18 04:03:41');

-- --------------------------------------------------------

--
-- Table structure for table `post-user-zones`
--

CREATE TABLE IF NOT EXISTS `post-user-zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '[F]riend or [M]eeting',
  `userid` int(11) NOT NULL COMMENT 'User setting up zone',
  `friendid` int(11) DEFAULT NULL COMMENT 'Friend Id if type [F]',
  `coords` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Coords if type [M]',
  `zonedistance` int(11) NOT NULL COMMENT 'Distance to trigger notification',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='User Zone table to store triggers' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `post-user-zones`
--

INSERT INTO `post-user-zones` (`id`, `type`, `userid`, `friendid`, `coords`, `zonedistance`) VALUES
(1, 'F', 1, 2, NULL, 25000),
(2, 'M', 2, 1, '50,-120', 300);

-- --------------------------------------------------------

--
-- Table structure for table `user-accounts`
--

CREATE TABLE IF NOT EXISTS `user-accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Phone Number',
  `auth` text COLLATE utf8_unicode_ci COMMENT 'OAuth Token',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='User Table' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user-accounts`
--

INSERT INTO `user-accounts` (`id`, `phone`, `auth`) VALUES
(1, '7027699388', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
